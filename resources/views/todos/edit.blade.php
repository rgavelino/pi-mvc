<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Editar</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">

    {{-- <h1 class="text-center my-5">{{$todo->name}}</h1>
    <p>{{ $todo->description}}</p> --}}
    <form>
        <div class="form-group">
          <label for="exampleFormControlInput1">Name</label>
          <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$todo->name}}">
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Description</label>
          <textarea class="form-control"  id="exampleFormControlTextarea1" rows="3">{{$todo->description}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </form>
    <a href="/todos">Voltar</a>
    </div>
</body>
</html>
